import requests
from flask import Flask, request, make_response, Response, render_template
from hashlib import sha256
import xml.etree.ElementTree as ET
import requests
import json

from werkzeug.exceptions import abort

from werkzeug.wrappers import ETagRequestMixin, ETagResponseMixin

app = Flask(__name__)

# Starts the web server
source = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"

data = requests.get(source)

root = ET.fromstring(data.text)


@app.route('/pools', methods = ['GET'])
def get_handler():
    pools = []
    for pool in root.findall('row'):
        dic = {}
        pool_name = pool.find('pool_name').text
        status = pool.find('status').text
        #phone = pool.find('phone').text
        open_date = pool.find('open_date').text
        pool_type = pool.find('pool_type').text
        weekday = pool.find('weekday').text
        weekend = pool.find('weekend').text
        dic['pool_name'] = pool_name
        dic['status'] = status
        #dic['phone'] = phone
        dic['open_date'] = open_date
        dic['pool_type'] = pool_type
        dic['weekday'] = weekday
        dic['weekend'] = weekend
        pools.append(dic)

    print(json.dumps(pools))
    return json.dumps(pools)

@app.route("/")
def pool_info_website():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
